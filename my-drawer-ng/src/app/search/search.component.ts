import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as Toast from "nativescript-toasts";
import { NoticiasService } from "../domain/noticias.service";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;

    constructor(private noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void
    {
        console.dir(x);
    }

    search(s: string)
    {
       //this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0); 
    }

    buscarAhora(s: string)
    {
        console.dir("BuscarAhora"+s);
        this.noticias.buscar(s).then((r: any)=>{
            console.log("resultados buscarAhora: "+JSON.stringify(r));
            this.resultados = r;
        }, (e)=>{
            console.log("error buscarAhora"+e);
            Toast.show({text: "Error de busqueda", duration: Toast.DURATION.SHORT});
        });
    }
}
